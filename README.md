# This project is script and data for knative experience.

### 代码
1. cold_start_individual.sh： 服务单独运行脚本
2. cold_start_2.sh：          2个服务单独运行脚本
3. cold_start_more.sh：       多个（4/8）服务单独运行脚本


### 目录
1. ./log          原始数据目录
2. ./filter_log   过滤后数据目录
3. ./filter_log/$/check_num.sh    检查log数量&是否完整
4. ./filter_log/$/get_$.py        convert log to csv
5. ./filter_log/$/result_$.csv    csv format result


### 实验
1. individual
2. multi_2(valid): 2
3. multi_4(valid): 3 & 4
4. multi_8(valid): 3 & 4 & 5 & 6 & 7 8 