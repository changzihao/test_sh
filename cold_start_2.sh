#!/bin/bash

declare -A t_language=(["go"]="helloworld-go" ["java-spring"]="helloworld-java-spring")
declare -A b_language=(["same-java-spring"]="helloworld-same-java-spring"
                     ["ruby"]="helloworld-ruby" ["nodejs"]="helloworld-nodejs" 
                     ["python"]="helloworld-python" ["php"]="helloworld-php" 
                     ["same-go"]="helloworld-same-go" ["java-spark"]="helloworld-java-spark")

# multi 2
mkdir -p ./log/multi_2
for victim in "go" "java-spring"
do
    for injurer in ${!b_language[@]}
    do
        for i in {1..15}
        do
            echo "call v: ${victim} i: ${injurer}, ${i} times"
            echo "call v: $victim i: $injurer, $i times" >> ./log/multi_2/$1_multi_2_$victim_$injurer.txt
            zsh -c "(time curl -s -H \"Host: ${b_language[$injurer]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_2/$1_multi_2_$victim_$injurer.txt &"
            zsh -c "(time python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l $victim -s ${t_language[$victim]}) &>> ./log/multi_2/$1_multi_2_$victim_$injurer.txt"
            echo "" >> ./log/multi_2/$1_multi_2_$victim_$injurer.txt
            sleep 110s
        done
    done
done
