#!/bin/bash

declare -A language=(["go"]="helloworld-go" ["java-spring"]="helloworld-java-spring")
# individual
mkdir -p ./log/individual
for name in "go" "java-spring"
do
    for i in {1..15}
    do
        echo "call $name individual $i"
        echo "call $name individual $i" >> ./log/individual/$1_individual_$name.txt
        zsh -c "(time python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l $name -s ${language[$name]}) &>> ./log/individual/$1_individual_$name.txt"
        echo "" >> ./log/individual/$1_individual_$name.txt
        sleep 110s

    done
done

