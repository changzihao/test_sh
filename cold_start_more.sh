#!/bin/bash

declare -A t_language=(["go"]="helloworld-go" ["java-spring"]="helloworld-java-spring")
declare -A b_language=(["same-java-spring"]="helloworld-same-java-spring"
                     ["ruby"]="helloworld-ruby" ["nodejs"]="helloworld-nodejs" 
                     ["python"]="helloworld-python" ["php"]="helloworld-php" 
                     ["same-go"]="helloworld-same-go" ["java-spark"]="helloworld-java-spark")

# multi more 
mkdir -p ./log/multi_$2
#for victim in "go" "java-spring"
for victim in "java-spring"
do
    for i in {1..15}
    do
        echo "call v: $victim $i times"
        echo "call v: $victim $i times" >> ./log/multi_$2/$1_multi_$2_$victim.txt
        zsh -c "(time curl -s -H \"Host: ${b_language["same-go"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
        zsh -c "(time curl -s -H \"Host: ${b_language["same-java-spring"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
        if [ $2 == 4 ]
        then
            python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l $victim -s ${t_language[$victim]} &>> ./log/multi_$2/$1_multi_$2_$victim.txt &
        fi
        zsh -c "(time curl -s -H \"Host: ${b_language["python"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
        if [ $2 == 8 ]
        then
            zsh -c "(time curl -s -H \"Host: ${b_language["nodejs"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
            python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l $victim -s ${t_language[$victim]} &>> ./log/multi_$2/$1_multi_$2_$victim.txt &
            zsh -c "(time curl -s -H \"Host: ${b_language["ruby"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
            zsh -c "(time curl -s -H \"Host: ${b_language["java-spark"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
            zsh -c "(time curl -s -H \"Host: ${b_language["php"]}.default.example.com\" http://172.18.13.250:31380 > /dev/null) &>> ./log/multi_$2/$1_multi_$2_$victim.txt &"
        fi
        echo "" >> ./log/multi_$2/$1_multi_$2_$victim.txt
        sleep 150s
     done
done
