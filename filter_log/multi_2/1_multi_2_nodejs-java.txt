call v: java-spring i: nodejs, 1 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 12.585 total
Cold start: e2e latency:  13.5715157 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.075344
kube-controller pod  latency:  12.562947
kubelet sandbox  latency:  1.408292
kubelet user-container  latency:  0.991346
kubelet queue-proxy  latency:  1.317129
user-container jvm  latency:  1.72
user-container app  latency:  9.6
user-container response  latency:  0.027
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.33s user 0.04s system 2% cpu 17.317 total

call v: java-spring i: nodejs, 4 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.028 total
Cold start: e2e latency:  8.72901331 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.080985
kube-controller pod  latency:  7.953824
kubelet sandbox  latency:  1.270094
kubelet user-container  latency:  0.899836
kubelet queue-proxy  latency:  1.201222
user-container jvm  latency:  1.12
user-container app  latency:  4.9
user-container response  latency:  0.027
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.03s system 2% cpu 12.951 total

call v: java-spring i: nodejs, 6 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 6.107 total
Cold start: e2e latency:  7.59223525 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.077057
kube-controller pod  latency:  6.869891
kubelet pod  latency:  6.774549
kubelet sandbox  latency:  1.122757
kubelet user-container  latency:  0.83159
kubelet queue-proxy  latency:  0.991316
user-container jvm  latency:  0.85
user-container app  latency:  4.45
user-container response  latency:  0.011
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.29s user 0.05s system 3% cpu 11.327 total

call v: java-spring i: nodejs, 7 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.991 total
Cold start: e2e latency:  9.01125889 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.039843
kube-controller pod  latency:  8.09797
kubelet sandbox  latency:  1.465615
kubelet user-container  latency:  0.817515
kubelet queue-proxy  latency:  2.326799
user-container jvm  latency:  1.44
user-container app  latency:  5.54
user-container response  latency:  0.011
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.34s user 0.02s system 2% cpu 12.747 total

call v: java-spring i: nodejs, 8 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.600 total
Cold start: e2e latency:  7.28805542 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.199982
kube-controller pod  latency:  6.36959
kubelet pod  latency:  6.123504
kubelet sandbox  latency:  1.151491
kubelet user-container  latency:  0.762825
kubelet queue-proxy  latency:  0.946428
user-container jvm  latency:  0.75
user-container app  latency:  3.7
user-container response  latency:  0.014
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.28s user 0.07s system 3% cpu 11.213 total

call v: java-spring i: nodejs, 9 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 11.782 total
Cold start: e2e latency:  16.80279159 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.160259
kube-controller pod  latency:  15.883209
kubelet pod  latency:  15.699921
kubelet sandbox  latency:  1.185828
kubelet user-container  latency:  1.323268
kubelet queue-proxy  latency:  3.759143
user-container jvm  latency:  4.88
user-container app  latency:  12.3
user-container response  latency:  0.044
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.05s system 1% cpu 21.326 total

call v: java-spring i: nodejs, 10 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 6.627 total
Cold start: e2e latency:  7.64819576 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.226747
kube-controller pod  latency:  6.747005
kubelet pod  latency:  6.504185
kubelet sandbox  latency:  1.200218
kubelet user-container  latency:  0.892726
kubelet queue-proxy  latency:  1.088863
user-container jvm  latency:  0.73
user-container app  latency:  4.1
user-container response  latency:  0.017
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.35s user 0.03s system 3% cpu 11.688 total

call v: java-spring i: nodejs, 11 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.959 total
Cold start: e2e latency:  7.48705943 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.014332
kube-controller pod  latency:  6.4681
kubelet pod  latency:  6.451892
kubelet sandbox  latency:  1.216296
kubelet user-container  latency:  0.823479
kubelet queue-proxy  latency:  1.083626
user-container jvm  latency:  0.74
user-container app  latency:  3.81
user-container response  latency:  0.009
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.34s user 0.03s system 3% cpu 11.485 total

call v: java-spring i: nodejs, 12 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 8.495 total
Cold start: e2e latency:  8.48292408 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.156364
kube-controller pod  latency:  6.803383
kubelet pod  latency:  6.653297
kubelet sandbox  latency:  1.467449
kubelet user-container  latency:  1.26672
kubelet queue-proxy  latency:  1.982872
user-container jvm  latency:  0.54
user-container app  latency:  3.64
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.03s system 2% cpu 11.963 total

call v: java-spring i: nodejs, 13 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.138 total
Cold start: e2e latency:  7.68071488 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.257044
kube-controller pod  latency:  6.556117
kubelet pod  latency:  6.291229
kubelet sandbox  latency:  1.505838
kubelet user-container  latency:  0.743757
kubelet queue-proxy  latency:  0.825562
user-container jvm  latency:  0.75
user-container app  latency:  3.67
user-container response  latency:  0.01
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.04s system 3% cpu 11.877 total

call v: java-spring i: nodejs, 14 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 6.184 total
Cold start: e2e latency:  7.68536411 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.069863
kube-controller pod  latency:  6.581831
kubelet pod  latency:  6.508196
kubelet sandbox  latency:  1.284243
kubelet user-container  latency:  1.246897
kubelet queue-proxy  latency:  0.885306
user-container jvm  latency:  0.61
user-container app  latency:  3.7
user-container response  latency:  0.009
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.05s system 3% cpu 11.832 total

call v: java-spring i: nodejs, 15 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.050 total
Cold start: e2e latency:  8.89169589 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.055756
kube-controller pod  latency:  8.327409
kubelet pod  latency:  8.258105
kubelet sandbox  latency:  1.262566
kubelet user-container  latency:  0.945639
kubelet queue-proxy  latency:  2.872039
user-container jvm  latency:  1.1
user-container app  latency:  4.5
user-container response  latency:  0.013
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.05s system 2% cpu 13.069 total

