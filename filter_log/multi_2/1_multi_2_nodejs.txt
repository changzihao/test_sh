call v: go i: nodejs, 1 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 10.167 total
Cold start: e2e latency:  5.71962340 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.150035
kube-controller pod  latency:  5.179245
kubelet pod  latency:  5.058807
kubelet sandbox  latency:  1.300607
kubelet user-container  latency:  0.726095
kubelet queue-proxy  latency:  0.860199
user-container app  latency:  0.000106
user-container response  latency:  0.001275
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.05s system 3% cpu 11.048 total

call v: go i: nodejs, 2 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 9.176 total
Cold start: e2e latency:  9.16457940 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.04852
kube-controller pod  latency:  7.53371
kubelet pod  latency:  7.470183
kubelet sandbox  latency:  1.581963
kubelet user-container  latency:  0.962934
kubelet queue-proxy  latency:  3.01771
user-container app  latency:  0.013761
user-container response  latency:  0.001995
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.05s system 2% cpu 13.570 total

call v: go i: nodejs, 3 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.142 total
Cold start: e2e latency:  7.13093012 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.055164
kube-controller pod  latency:  5.276721
kubelet pod  latency:  5.220051
kubelet sandbox  latency:  1.527435
kubelet user-container  latency:  0.721008
kubelet queue-proxy  latency:  0.670648
user-container app  latency:  0.000105
user-container response  latency:  0.001373
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.06s system 3% cpu 11.569 total

call v: go i: nodejs, 4 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.120 total
Cold start: e2e latency:  5.95427615 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.412821
kube-controller pod  latency:  5.489583
kubelet pod  latency:  5.068187
kubelet sandbox  latency:  1.312316
kubelet user-container  latency:  0.917177
kubelet queue-proxy  latency:  0.723568
user-container app  latency:  0.000104
user-container response  latency:  0.003989
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.34s user 0.05s system 3% cpu 11.044 total

call v: go i: nodejs, 5 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 6.107 total
Cold start: e2e latency:  6.09501292 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.307707
kube-controller pod  latency:  4.604183
kubelet sandbox  latency:  1.251317
kubelet user-container  latency:  0.836837
kubelet queue-proxy  latency:  0.740883
user-container app  latency:  9.3e-05
user-container response  latency:  0.001033
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.03s system 3% cpu 10.278 total

call v: go i: nodejs, 6 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 6.772 total
Cold start: e2e latency:  6.76060432 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.224648
kube-controller pod  latency:  5.288495
kubelet sandbox  latency:  1.321072
kubelet user-container  latency:  0.760983
kubelet queue-proxy  latency:  0.691726
user-container app  latency:  0.000103
user-container response  latency:  0.001585
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.05s system 3% cpu 10.957 total

call v: go i: nodejs, 7 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 6.601 total
Cold start: e2e latency:  5.96578861 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.056776
kube-controller pod  latency:  5.220449
kubelet pod  latency:  5.166294
kubelet sandbox  latency:  1.349978
kubelet user-container  latency:  0.674997
kubelet queue-proxy  latency:  0.753015
user-container app  latency:  9.3e-05
user-container response  latency:  0.001106
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.32s user 0.03s system 3% cpu 11.302 total

call v: go i: nodejs, 8 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.351 total
Cold start: e2e latency:  6.33945463 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.069455
kube-controller pod  latency:  4.623258
kubelet pod  latency:  4.555942
kubelet sandbox  latency:  1.219013
kubelet user-container  latency:  0.804494
kubelet queue-proxy  latency:  0.944375
user-container app  latency:  0.000111
user-container response  latency:  0.007463
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.03s system 3% cpu 10.253 total

call v: go i: nodejs, 9 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.594 total
Cold start: e2e latency:  8.58214655 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.061623
kube-controller pod  latency:  6.903108
kubelet sandbox  latency:  1.61118
kubelet user-container  latency:  0.661898
kubelet queue-proxy  latency:  2.173075
user-container app  latency:  0.000108
user-container response  latency:  0.001795
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.32s user 0.03s system 2% cpu 12.812 total

call v: go i: nodejs, 10 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 6.411 total
Cold start: e2e latency:  6.39796077 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.164258
kube-controller pod  latency:  4.677443
kubelet pod  latency:  4.507559
kubelet sandbox  latency:  1.28529
kubelet user-container  latency:  0.916148
kubelet queue-proxy  latency:  0.843412
user-container app  latency:  0.000776
user-container response  latency:  0.000996
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.04s system 3% cpu 10.424 total

call v: go i: nodejs, 11 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 7.875 total
Cold start: e2e latency:  7.86288632 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.068277
kube-controller pod  latency:  6.26011
kubelet pod  latency:  6.174526
kubelet sandbox  latency:  1.547902
kubelet user-container  latency:  1.299969
kubelet queue-proxy  latency:  2.035805
user-container app  latency:  0.000125
user-container response  latency:  0.001031
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.34s user 0.04s system 3% cpu 12.252 total

call v: go i: nodejs, 12 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 5.589 total
Cold start: e2e latency:  5.57724158 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.092354
kube-controller pod  latency:  3.912117
kubelet pod  latency:  3.8089
kubelet sandbox  latency:  1.431843
kubelet user-container  latency:  0.702127
kubelet queue-proxy  latency:  0.623273
user-container app  latency:  0.000112
user-container response  latency:  0.002549
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.08s system 4% cpu 9.868 total

call v: go i: nodejs, 13 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.731 total
Cold start: e2e latency:  7.18378670 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.293348
kube-controller pod  latency:  6.171478
kubelet pod  latency:  5.860753
kubelet sandbox  latency:  1.551005
kubelet user-container  latency:  0.674775
kubelet queue-proxy  latency:  0.649181
user-container app  latency:  0.000105
user-container response  latency:  0.030601
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.34s user 0.05s system 3% cpu 12.958 total

call v: go i: nodejs, 14 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.539 total
Cold start: e2e latency:  9.5273752 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.179691
kube-controller pod  latency:  7.889031
kubelet pod  latency:  7.697869
kubelet sandbox  latency:  1.665325
kubelet user-container  latency:  0.814067
kubelet queue-proxy  latency:  1.824654
user-container app  latency:  0.000209
user-container response  latency:  0.001266
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.32s user 0.06s system 2% cpu 13.886 total

call v: go i: nodejs, 15 times
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.349 total
Cold start: e2e latency:  6.94332486 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.225279
kube-controller pod  latency:  6.127682
kubelet pod  latency:  5.874955
kubelet sandbox  latency:  1.669454
kubelet user-container  latency:  0.634202
kubelet queue-proxy  latency:  0.79342
user-container app  latency:  0.004359
user-container response  latency:  0.00136
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.04s system 3% cpu 12.139 total
