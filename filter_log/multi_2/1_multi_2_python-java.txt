call v: java-spring i: python, 1 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 7.381 total
Cold start: e2e latency:  8.7469232 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.256579
kube-controller pod  latency:  7.735176
kubelet sandbox  latency:  1.535954
kubelet user-container  latency:  1.464103
kubelet queue-proxy  latency:  2.207481
user-container jvm  latency:  1.18
user-container app  latency:  4.6
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.33s user 0.02s system 2% cpu 13.042 total

call v: java-spring i: python, 2 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 5.740 total
Cold start: e2e latency:  7.64907207 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.168953
kube-controller pod  latency:  6.684522
kubelet sandbox  latency:  1.288943
kubelet user-container  latency:  0.950795
kubelet queue-proxy  latency:  1.024931
user-container jvm  latency:  0.73
user-container app  latency:  3.89
user-container response  latency:  0.013
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.34s user 0.03s system 2% cpu 12.519 total

call v: java-spring i: python, 3 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 6.589 total
Cold start: e2e latency:  8.25176047 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.190887
kube-controller pod  latency:  7.440465
kubelet sandbox  latency:  1.248498
kubelet user-container  latency:  0.830401
kubelet queue-proxy  latency:  1.104388
user-container jvm  latency:  0.63
user-container app  latency:  4.0
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.03s system 2% cpu 13.164 total

call v: java-spring i: python, 4 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.925 total
Cold start: e2e latency:  8.48156613 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.332115
kube-controller pod  latency:  7.718005
kubelet sandbox  latency:  1.703617
kubelet user-container  latency:  1.23353
kubelet queue-proxy  latency:  1.073186
user-container jvm  latency:  1.25
user-container app  latency:  4.24
user-container response  latency:  0.02
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.05s system 2% cpu 13.023 total

call v: java-spring i: python, 5 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 6.780 total
Cold start: e2e latency:  7.37181280 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.190801
kube-controller pod  latency:  6.528165
kubelet sandbox  latency:  1.469849
kubelet user-container  latency:  0.789249
kubelet queue-proxy  latency:  1.202333
user-container jvm  latency:  0.85
user-container app  latency:  3.73
user-container response  latency:  0.009
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.03s system 2% cpu 11.752 total

call v: java-spring i: python, 6 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.303 total
Cold start: e2e latency:  9.4792980 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.242123
kube-controller pod  latency:  8.760665
kubelet sandbox  latency:  1.932245
kubelet user-container  latency:  1.384593
kubelet queue-proxy  latency:  2.469083
user-container jvm  latency:  2.03
user-container app  latency:  5.2
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.03s system 2% cpu 13.836 total

call v: java-spring i: python, 7 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 8.644 total
Cold start: e2e latency:  8.85523857 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.195635
kube-controller pod  latency:  7.847411
kubelet sandbox  latency:  1.277349
kubelet user-container  latency:  0.800014
kubelet queue-proxy  latency:  1.564843
user-container jvm  latency:  1.0
user-container app  latency:  4.01
user-container response  latency:  0.023
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.05s system 2% cpu 13.104 total

call v: java-spring i: python, 8 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 9.676 total
Cold start: e2e latency:  13.32633863 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.263704
kube-controller pod  latency:  12.616835
kubelet pod  latency:  12.336922
kubelet sandbox  latency:  1.423176
kubelet user-container  latency:  1.058511
kubelet queue-proxy  latency:  3.497734
user-container jvm  latency:  4.07
user-container app  latency:  8.9
user-container response  latency:  0.015
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.36s user 0.04s system 2% cpu 17.760 total

call v: java-spring i: python, 9 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 6.198 total
Cold start: e2e latency:  7.74627937 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.236478
kube-controller pod  latency:  6.821303
kubelet pod  latency:  6.59032
kubelet sandbox  latency:  1.536909
kubelet user-container  latency:  0.779528
kubelet queue-proxy  latency:  0.880107
user-container jvm  latency:  0.61
user-container app  latency:  3.88
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.06s system 3% cpu 12.465 total

call v: java-spring i: python, 10 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 6.248 total
Cold start: e2e latency:  8.09600996 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.265051
kube-controller pod  latency:  7.258731
kubelet pod  latency:  7.009354
kubelet sandbox  latency:  1.678984
kubelet user-container  latency:  0.658331
kubelet queue-proxy  latency:  0.84096
user-container jvm  latency:  0.62
user-container app  latency:  3.79
user-container response  latency:  0.012
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.04s system 2% cpu 12.668 total

call v: java-spring i: python, 11 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.124 total
Cold start: e2e latency:  7.41803449 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.149681
kube-controller pod  latency:  6.440588
kubelet pod  latency:  6.277073
kubelet sandbox  latency:  1.581814
kubelet user-container  latency:  0.691979
kubelet queue-proxy  latency:  0.95296
user-container jvm  latency:  0.63
user-container app  latency:  3.51
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.05s system 2% cpu 12.227 total

call v: java-spring i: python, 12 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 5.416 total
Cold start: e2e latency:  7.38144235 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.265774
kube-controller pod  latency:  6.521529
kubelet pod  latency:  6.25644
kubelet sandbox  latency:  1.096016
kubelet user-container  latency:  0.806645
kubelet queue-proxy  latency:  1.052465
user-container jvm  latency:  0.69
user-container app  latency:  4.0
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.04s system 2% cpu 11.989 total

call v: java-spring i: python, 13 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.084 total
Cold start: e2e latency:  7.21334694 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.158937
kube-controller pod  latency:  6.549702
kubelet pod  latency:  6.369508
kubelet sandbox  latency:  1.264757
kubelet user-container  latency:  0.812631
kubelet queue-proxy  latency:  1.002145
user-container jvm  latency:  0.67
user-container app  latency:  3.67
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.28s user 0.05s system 2% cpu 12.314 total

call v: java-spring i: python, 15 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 6.414 total
Cold start: e2e latency:  7.43412437 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.054548
kube-controller pod  latency:  6.549636
kubelet sandbox  latency:  1.444843
kubelet user-container  latency:  0.911911
kubelet queue-proxy  latency:  0.740594
user-container jvm  latency:  0.62
user-container app  latency:  3.6
user-container response  latency:  0.018
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.04s system 2% cpu 12.169 total

