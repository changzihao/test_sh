call v: java-spring i: same-java-spring, 1 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 12.019 total
Cold start: e2e latency:  12.00743714 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.189553
kube-controller pod  latency:  10.240243
kubelet pod  latency:  10.034335
kubelet sandbox  latency:  1.629095
kubelet user-container  latency:  0.638103
kubelet queue-proxy  latency:  1.462608
user-container jvm  latency:  1.74
user-container app  latency:  6.89
user-container response  latency:  0.033
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.34s user 0.03s system 2% cpu 14.723 total

call v: java-spring i: same-java-spring, 2 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 10.441 total
Cold start: e2e latency:  10.13040099 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.163403
kube-controller pod  latency:  9.139656
kubelet pod  latency:  8.967766
kubelet sandbox  latency:  1.38995
kubelet user-container  latency:  1.598569
kubelet queue-proxy  latency:  1.372405
user-container jvm  latency:  1.04
user-container app  latency:  5.61
user-container response  latency:  0.017
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.03s system 2% cpu 13.331 total

call v: java-spring i: same-java-spring, 3 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.898 total
Cold start: e2e latency:  10.88699608 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.016899
kube-controller pod  latency:  8.646563
kubelet pod  latency:  8.627402
kubelet sandbox  latency:  1.154099
kubelet user-container  latency:  0.942478
kubelet queue-proxy  latency:  1.344586
user-container jvm  latency:  2.02
user-container app  latency:  6.53
user-container response  latency:  0.009
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.04s system 2% cpu 13.806 total

call v: java-spring i: same-java-spring, 4 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.340 total
Cold start: e2e latency:  10.21227203 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.175371
kube-controller pod  latency:  9.271188
kubelet sandbox  latency:  1.390147
kubelet user-container  latency:  0.758156
kubelet queue-proxy  latency:  1.24055
user-container jvm  latency:  1.2
user-container app  latency:  5.61
user-container response  latency:  0.01
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.04s system 2% cpu 13.321 total

call v: java-spring i: same-java-spring, 5 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 13.285 total
Cold start: e2e latency:  13.27296737 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.363967
kube-controller pod  latency:  11.451431
kubelet sandbox  latency:  1.492641
kubelet user-container  latency:  0.823689
kubelet queue-proxy  latency:  1.1568
user-container jvm  latency:  2.46
user-container app  latency:  7.38
user-container response  latency:  0.035
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.28s user 0.06s system 2% cpu 15.499 total

call v: java-spring i: same-java-spring, 6 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.668 total
Cold start: e2e latency:  10.72890097 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.039261
kube-controller pod  latency:  9.663028
kubelet pod  latency:  9.603957
kubelet sandbox  latency:  1.240892
kubelet user-container  latency:  0.660426
kubelet queue-proxy  latency:  1.092181
user-container jvm  latency:  1.22
user-container app  latency:  5.76
user-container response  latency:  0.008
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.05s system 2% cpu 14.011 total

call v: java-spring i: same-java-spring, 7 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 10.076 total
Cold start: e2e latency:  10.06411012 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.283498
kube-controller pod  latency:  8.122514
kubelet pod  latency:  7.820594
kubelet sandbox  latency:  1.330363
kubelet user-container  latency:  0.792212
kubelet queue-proxy  latency:  1.047877
user-container jvm  latency:  0.94
user-container app  latency:  5.14
user-container response  latency:  0.019
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.05s system 2% cpu 12.766 total

call v: java-spring i: same-java-spring, 8 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 11.923 total
Cold start: e2e latency:  11.91196707 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.25609
kube-controller pod  latency:  10.154978
kubelet pod  latency:  9.923253
kubelet sandbox  latency:  1.585154
kubelet user-container  latency:  1.641824
kubelet queue-proxy  latency:  1.028231
user-container jvm  latency:  1.5
user-container app  latency:  6.62
user-container response  latency:  0.014
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.02s system 2% cpu 14.256 total

call v: java-spring i: same-java-spring, 9 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 10.998 total
Cold start: e2e latency:  10.98637151 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.355935
kube-controller pod  latency:  9.369307
kubelet pod  latency:  8.990663
kubelet sandbox  latency:  1.333839
kubelet user-container  latency:  0.734106
kubelet queue-proxy  latency:  1.11956
user-container jvm  latency:  1.84
user-container app  latency:  6.54
user-container response  latency:  0.028
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.05s system 2% cpu 13.418 total

call v: java-spring i: same-java-spring, 10 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.301 total
Cold start: e2e latency:  10.28924311 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.268695
kube-controller pod  latency:  8.59338
kubelet pod  latency:  8.323285
kubelet sandbox  latency:  1.434731
kubelet user-container  latency:  1.211242
kubelet queue-proxy  latency:  1.513973
user-container jvm  latency:  0.69
user-container app  latency:  5.4
user-container response  latency:  0.017
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.32s user 0.03s system 2% cpu 13.425 total

call v: java-spring i: same-java-spring, 11 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 11.333 total
Cold start: e2e latency:  10.78230279 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.164089
kube-controller pod  latency:  10.047808
kubelet pod  latency:  9.876621
kubelet sandbox  latency:  1.250993
kubelet user-container  latency:  0.795311
kubelet queue-proxy  latency:  1.02811
user-container jvm  latency:  2.37
user-container app  latency:  7.24
user-container response  latency:  0.022
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.34s user 0.04s system 2% cpu 14.560 total

call v: java-spring i: same-java-spring, 12 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 9.928 total
Cold start: e2e latency:  9.84751845 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.236931
kube-controller pod  latency:  9.136003
kubelet pod  latency:  8.88155
kubelet sandbox  latency:  1.271049
kubelet user-container  latency:  0.997079
kubelet queue-proxy  latency:  1.175478
user-container jvm  latency:  1.3
user-container app  latency:  5.7
user-container response  latency:  0.019
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.30s user 0.06s system 2% cpu 13.290 total

call v: java-spring i: same-java-spring, 13 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 12.321 total
Cold start: e2e latency:  12.20610119 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.285924
kube-controller pod  latency:  10.777553
kubelet pod  latency:  10.470878
kubelet sandbox  latency:  1.323132
kubelet user-container  latency:  1.107083
kubelet queue-proxy  latency:  1.369508
user-container jvm  latency:  2.37
user-container app  latency:  7.45
user-container response  latency:  0.017
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.04s system 2% cpu 15.572 total

call v: java-spring i: same-java-spring, 14 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 11.097 total
Cold start: e2e latency:  11.08604354 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.067178
kube-controller pod  latency:  9.169969
kubelet pod  latency:  9.081122
kubelet sandbox  latency:  1.495039
kubelet user-container  latency:  0.781013
kubelet queue-proxy  latency:  0.866193
user-container jvm  latency:  1.24
user-container app  latency:  5.73
user-container response  latency:  0.013
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.34s user 0.04s system 2% cpu 13.636 total

call v: java-spring i: same-java-spring, 15 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 9.321 total
Cold start: e2e latency:  9.34880623 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.068317
kube-controller pod  latency:  8.258389
kubelet pod  latency:  8.187091
kubelet sandbox  latency:  1.300478
kubelet user-container  latency:  1.07872
kubelet queue-proxy  latency:  1.083451
user-container jvm  latency:  0.88
user-container app  latency:  5.32
user-container response  latency:  0.01
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l java-spring -  0.31s user 0.04s system 2% cpu 12.189 total

