call v: go i: same-java-spring, 1 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 7.883 total
Cold start: e2e latency:  7.87100010 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.066705
kube-controller pod  latency:  5.20936
kubelet sandbox  latency:  1.300012
kubelet user-container  latency:  0.833812
kubelet queue-proxy  latency:  0.895666
user-container app  latency:  0.000136
user-container response  latency:  0.013539
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.03s system 3% cpu 9.954 total

call v: go i: same-java-spring, 2 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 7.840 total
Cold start: e2e latency:  7.82805587 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.081888
kube-controller pod  latency:  4.844032
kubelet sandbox  latency:  1.201716
kubelet user-container  latency:  0.822753
kubelet queue-proxy  latency:  1.165408
user-container app  latency:  0.000113
user-container response  latency:  0.001446
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.29s user 0.04s system 3% cpu 9.663 total

call v: go i: same-java-spring, 4 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 8.029 total
Cold start: e2e latency:  8.01698854 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.531974
kube-controller pod  latency:  5.272055
kubelet sandbox  latency:  1.266465
kubelet user-container  latency:  0.832726
kubelet queue-proxy  latency:  1.029597
user-container app  latency:  0.000104
user-container response  latency:  0.001403
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.04s system 3% cpu 10.279 total

call v: go i: same-java-spring, 5 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 9.362 total
Cold start: e2e latency:  9.3501206 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.54463
kube-controller pod  latency:  5.845073
kubelet sandbox  latency:  1.43762
kubelet user-container  latency:  0.774581
kubelet queue-proxy  latency:  0.975752
user-container app  latency:  0.000351
user-container response  latency:  0.000915
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.04s system 3% cpu 11.185 total

call v: go i: same-java-spring, 6 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 8.696 total
Cold start: e2e latency:  8.68518312 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.265362
kube-controller pod  latency:  6.608657
kubelet sandbox  latency:  1.507244
kubelet user-container  latency:  0.845953
kubelet queue-proxy  latency:  1.001214
user-container app  latency:  0.000106
user-container response  latency:  0.007024
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.04s system 3% cpu 11.643 total

call v: go i: same-java-spring, 7 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 8.448 total
Cold start: e2e latency:  8.43624333 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.055149
kube-controller pod  latency:  6.371357
kubelet pod  latency:  6.295436
kubelet sandbox  latency:  1.457189
kubelet user-container  latency:  0.670447
kubelet queue-proxy  latency:  1.01598
user-container app  latency:  0.0001
user-container response  latency:  0.002352
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.04s system 3% cpu 11.713 total

call v: go i: same-java-spring, 8 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 7.503 total
Cold start: e2e latency:  8.59330666 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.195692
kube-controller pod  latency:  7.658908
kubelet pod  latency:  7.464379
kubelet sandbox  latency:  1.193345
kubelet user-container  latency:  1.301847
kubelet queue-proxy  latency:  2.129069
user-container app  latency:  0.000101
user-container response  latency:  0.00706
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.03s system 2% cpu 12.692 total

call v: go i: same-java-spring, 9 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 6.756 total
Cold start: e2e latency:  6.45564724 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.291026
kube-controller pod  latency:  5.982346
kubelet pod  latency:  5.67209
kubelet sandbox  latency:  1.137422
kubelet user-container  latency:  1.113555
kubelet queue-proxy  latency:  0.863409
user-container app  latency:  0.000108
user-container response  latency:  0.001241
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.32s user 0.05s system 3% cpu 10.626 total

call v: go i: same-java-spring, 10 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.01s system 0% cpu 9.777 total
Cold start: e2e latency:  9.76404212 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.08275
kube-controller pod  latency:  5.724062
kubelet pod  latency:  5.623169
kubelet sandbox  latency:  2.612672
kubelet user-container  latency:  0.896313
kubelet queue-proxy  latency:  0.952207
user-container app  latency:  0.000103
user-container response  latency:  0.012408
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.32s user 0.04s system 3% cpu 10.839 total

call v: go i: same-java-spring, 11 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 7.354 total
Cold start: e2e latency:  7.34141265 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.183203
kube-controller pod  latency:  5.584754
kubelet pod  latency:  5.391383
kubelet sandbox  latency:  1.122173
kubelet user-container  latency:  0.914209
kubelet queue-proxy  latency:  0.784468
user-container app  latency:  0.00013
user-container response  latency:  0.000929
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.29s user 0.06s system 3% cpu 10.785 total

call v: go i: same-java-spring, 12 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 8.495 total
Cold start: e2e latency:  8.48344264 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.272325
kube-controller pod  latency:  6.080097
kubelet pod  latency:  5.821149
kubelet sandbox  latency:  1.450732
kubelet user-container  latency:  0.782362
kubelet queue-proxy  latency:  1.01287
user-container app  latency:  0.000107
user-container response  latency:  0.002418
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.05s system 3% cpu 11.211 total

call v: go i: same-java-spring, 13 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 7.288 total
Cold start: e2e latency:  7.27646940 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.249126
kube-controller pod  latency:  4.783176
kubelet pod  latency:  4.546772
kubelet sandbox  latency:  1.407359
kubelet user-container  latency:  0.659264
kubelet queue-proxy  latency:  0.948682
user-container app  latency:  0.000204
user-container response  latency:  0.001233
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.03s system 3% cpu 9.700 total

call v: go i: same-java-spring, 14 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 7.351 total
Cold start: e2e latency:  7.34005991 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.319632
kube-controller pod  latency:  5.429498
kubelet pod  latency:  5.102465
kubelet sandbox  latency:  1.307307
kubelet user-container  latency:  0.930551
kubelet queue-proxy  latency:  0.873721
user-container app  latency:  0.000107
user-container response  latency:  0.001395
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.03s system 3% cpu 10.995 total

call v: go i: same-java-spring, 15 times
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 8.176 total
Cold start: e2e latency:  8.16506422 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.225348
kube-controller pod  latency:  4.998469
kubelet pod  latency:  4.783053
kubelet sandbox  latency:  1.237498
kubelet user-container  latency:  0.679462
kubelet queue-proxy  latency:  0.738581
user-container app  latency:  9.2e-05
user-container response  latency:  0.002122
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.28s user 0.05s system 3% cpu 10.352 total