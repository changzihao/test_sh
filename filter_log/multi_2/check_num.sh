#!/bin/bash

cd /Users/czh/Documents/multi_2

for file in $(ls ./)
do 
  echo "file : $file"
  for it in "Cold" "scheduler" "sandbox" "kubelet queue" 'kubelet user' "app"
  do
    num=$(cat $file | grep "$it" | wc -l)
    echo "$it : $num" 
  done
  echo ""
done
