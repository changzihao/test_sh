import subprocess

files = subprocess.getoutput("ls").split('\n')
# print(files)

f_o = open('result_8.csv', 'w')

for f in files:
    if f.find('txt') != -1:
        f_o.write(f+'\n')
        f_o.write("Cold,scheduler,sandbox,kubelet-user,kubelet-queue,app\n")
        cold = subprocess.getoutput("cat {} ".format(f) + "| grep Cold | awk '{print $5}'").split('\n')
        scheduler = subprocess.getoutput("cat {} ".format(f) + "| grep scheduler | awk '{print $4}'").split('\n')
        sandbox = subprocess.getoutput("cat {} ".format(f) + "| grep sandbox | awk '{print $4}'").split('\n')
        user = subprocess.getoutput("cat {} ".format(f) + "| grep 'kubelet user' | awk '{print $4}'").split('\n')
        queue = subprocess.getoutput("cat {} ".format(f) + "| grep 'kubelet queue' | awk '{print $4}'").split('\n')
        app = subprocess.getoutput("cat {} ".format(f) + "| grep 'app' | awk '{print $4}'").split('\n')
    
        for i in range(0, len(cold)):
            # print(i)
            f_o.write('{},{},{},{},{},{}\n'.format(cold[i], scheduler[i], sandbox[i], user[i], queue[i],app[i]))
    
        f_o.write("\n")
