call v: go 1 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 7.568 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 8.768 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 9.327 total
Cold start: e2e latency:  9.3151714 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.270857
kube-controller pod  latency:  6.386042
kubelet sandbox  latency:  1.303835
user-container app  latency:  0.024938
user-container response  latency:  0.00103
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.28s user 0.05s system 2% cpu 12.558 total

call v: go 2 times
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.927 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 8.670 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 10.744 total
Cold start: e2e latency:  10.73344065599999 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.189842
kube-controller pod  latency:  6.694182
kubelet sandbox  latency:  2.047307
kubelet user-container  latency:  1.30886
user-container app  latency:  0.000139
user-container response  latency:  0.00183
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.04s system 2% cpu 12.384 total

call v: go 3 times
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 8.129 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 8.763 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.056 total
Cold start: e2e latency:  10.04583732 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.294967
kube-controller pod  latency:  6.223193
kubelet sandbox  latency:  1.67726
kubelet user-container  latency:  1.11605
user-container app  latency:  0.000102
user-container response  latency:  0.005192
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.03s system 2% cpu 12.212 total

call v: go 4 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.151 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.754 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 11.952 total
Cold start: e2e latency:  11.94115554 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.052565
kube-controller pod  latency:  7.903684
kubelet pod  latency:  7.815124
Traceback (most recent call last):
  File "/home/czh/projects/knative-trace/getTrace.py", line 144, in <module>
    get_trace(args)
  File "/home/czh/projects/knative-trace/getTrace.py", line 125, in get_trace
    output.print_mini(mytrace)
  File "/home/czh/projects/knative-trace/output.py", line 13, in print_mini
    time = (event.ts - log.events['start'].ts).total_seconds()
KeyError: 'start'
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.35s user 0.05s system 2% cpu 14.320 total

call v: go 5 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 10.548 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.800 total
Cold start: e2e latency:  13.78740028 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.283359
kube-controller pod  latency:  10.838853
kubelet sandbox  latency:  1.50155
kubelet user-container  latency:  0.616931
user-container app  latency:  0.01361
user-container response  latency:  0.008323
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.33s user 0.03s system 2% cpu 17.975 total

curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 18.662 total
call v: go 6 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 8.982 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.440 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 10.938 total
Cold start: e2e latency:  10.92767193 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.270504
kube-controller pod  latency:  7.101498
user-container app  latency:  0.000178
user-container response  latency:  0.00118
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.04s system 2% cpu 13.020 total

call v: go 7 times
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.320 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.269 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 11.590 total
Cold start: e2e latency:  11.57988995 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.285283
kube-controller pod  latency:  7.149635
kubelet sandbox  latency:  1.492217
kubelet user-container  latency:  0.888346
kubelet queue-proxy  latency:  1.171411
user-container app  latency:  0.012364
user-container response  latency:  0.002306
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.34s user 0.02s system 2% cpu 14.623 total

call v: go 8 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.746 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 11.208 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 11.876 total
Cold start: e2e latency:  11.86498488 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.074803
kube-controller pod  latency:  8.427082
kubelet sandbox  latency:  1.476741
kubelet user-container  latency:  0.364079
kubelet queue-proxy  latency:  1.799855
user-container app  latency:  0.000107
user-container response  latency:  0.002131
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.05s system 2% cpu 16.003 total

call v: go 9 times
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 7.500 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.837 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 10.191 total
Cold start: e2e latency:  10.17706797 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.25076
kube-controller pod  latency:  6.129505
kubelet pod  latency:  5.849797
kubelet sandbox  latency:  1.904003
kubelet user-container  latency:  0.944767
kubelet queue-proxy  latency:  1.603561
user-container app  latency:  0.011764
user-container response  latency:  0.002728
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.34s user 0.07s system 3% cpu 12.435 total

call v: go 10 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.558 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.194 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.814 total
Cold start: e2e latency:  10.80395984 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.11345
kube-controller pod  latency:  8.121745
kubelet pod  latency:  8.035795
kubelet sandbox  latency:  2.28324
kubelet user-container  latency:  1.549471
kubelet queue-proxy  latency:  1.079631
user-container app  latency:  0.000106
user-container response  latency:  0.000939
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.36s user 0.05s system 2% cpu 15.399 total

call v: go 11 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.193 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 10.135 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.359 total
Cold start: e2e latency:  10.34729936 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.067375
kube-controller pod  latency:  8.709233
kubelet sandbox  latency:  2.133345
kubelet user-container  latency:  2.332863
kubelet queue-proxy  latency:  1.103188
user-container app  latency:  0.000104
user-container response  latency:  0.00147
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.30s user 0.06s system 2% cpu 15.011 total

call v: go 12 times
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 11.233 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 11.471 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 12.751 total
Cold start: e2e latency:  12.7403448 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.249727
kube-controller pod  latency:  10.792921
kubelet sandbox  latency:  2.165642
kubelet user-container  latency:  3.153441
kubelet queue-proxy  latency:  1.197577
user-container app  latency:  0.000118
user-container response  latency:  0.002446
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.34s user 0.04s system 2% cpu 17.241 total

call v: go 13 times
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.429 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.290 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 11.082 total
Cold start: e2e latency:  11.07131883 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.284036
kube-controller pod  latency:  8.764531
kubelet sandbox  latency:  1.792244
kubelet user-container  latency:  1.49819
user-container app  latency:  0.024707
user-container response  latency:  0.003973
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.32s user 0.01s system 2% cpu 15.247 total

call v: go 14 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.487 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 10.784 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 12.320 total
Cold start: e2e latency:  12.3091618 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.150268
kube-controller pod  latency:  8.333727
kubelet sandbox  latency:  2.178951
kubelet user-container  latency:  3.191207
user-container app  latency:  0.0001
user-container response  latency:  0.010402
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.31s user 0.05s system 2% cpu 15.005 total

call v: go 15 times
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.067 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.419 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 12.465 total
Cold start: e2e latency:  10.24452479 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.366111
Traceback (most recent call last):
  File "/home/czh/projects/knative-trace/getTrace.py", line 144, in <module>
    get_trace(args)
  File "/home/czh/projects/knative-trace/getTrace.py", line 125, in get_trace
    output.print_mini(mytrace)
  File "/home/czh/projects/knative-trace/output.py", line 13, in print_mini
    time = (event.ts - log.events['start'].ts).total_seconds()
KeyError: 'start'
python3 /home/czh/projects/knative-trace/getTrace.py -o mini -l go -s   0.36s user 0.04s system 3% cpu 13.172 total

