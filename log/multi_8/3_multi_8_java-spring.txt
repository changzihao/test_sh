call v: java-spring 1 times

curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 10.714 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.411 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 13.777 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 14.482 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 14.687 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 15.353 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 15.550 total
request time latency is : 15.694292
Cold start: e2e latency:  15.68924021 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.048647
kube-controller pod  latency:  14.304924
user-container jvm  latency:  1.33
user-container app  latency:  7.12
user-container response  latency:  0.024
call v: java-spring 2 times

curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.867 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.173 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 13.534 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.761 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 14.212 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.721 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 15.768 total
request time latency is : 14.838582
Cold start: e2e latency:  15.75738918 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.250281
kube-controller pod  latency:  13.2936
user-container jvm  latency:  1.14
user-container app  latency:  7.35
user-container response  latency:  0.011
call v: java-spring 3 times

curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 11.338 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.677 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 15.814 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 16.302 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 16.393 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 17.194 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 17.607 total
request time latency is : 16.978862
Cold start: e2e latency:  17.59542788 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.164863
kube-controller pod  latency:  14.79904
user-container jvm  latency:  1.28
user-container app  latency:  6.46
user-container response  latency:  0.031
call v: java-spring 4 times

curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 11.396 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 11.733 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.173 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.537 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 13.152 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.963 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 16.275 total
request time latency is : 15.195699
Cold start: e2e latency:  16.26390417 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.188755
kube-controller pod  latency:  13.909456
user-container jvm  latency:  1.33
user-container app  latency:  7.8
user-container response  latency:  0.017
call v: java-spring 5 times

curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 13.335 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.272 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 14.645 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 16.601 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 17.052 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 17.477 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 18.132 total
request time latency is : 17.577844
Cold start: e2e latency:  17.57287819 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.235787
kube-controller pod  latency:  15.843149
call v: java-spring 6 times

curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 13.071 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 13.064 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.947 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 16.515 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 16.989 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 18.275 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 18.923 total
request time latency is : 15.324599
Cold start: e2e latency:  18.91160681 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.196772
kube-controller pod  latency:  13.680559
call v: java-spring 7 times

curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 8.782 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.270 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.917 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.527 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.961 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.648 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 15.630 total
request time latency is : 15.003088
Cold start: e2e latency:  15.61846229 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.052199
kube-controller pod  latency:  12.932116
user-container jvm  latency:  1.19
user-container app  latency:  7.48
user-container response  latency:  0.049
call v: java-spring 8 times

curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.951 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 16.638 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 17.137 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 17.284 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 18.128 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 18.711 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 19.008 total
request time latency is : 13.021334
Cold start: e2e latency:  17.27332056 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.209749
kube-controller pod  latency:  11.38227
kubelet pod  latency:  11.152572
user-container jvm  latency:  1.13
user-container app  latency:  6.4
user-container response  latency:  0.023
call v: java-spring 9 times

curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 9.257 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 14.274 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 14.422 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.706 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 14.945 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 15.300 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 17.202 total
request time latency is : 15.293923
Cold start: e2e latency:  17.19004570 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.009698
kube-controller pod  latency:  13.306178
user-container jvm  latency:  1.6
user-container app  latency:  6.95
user-container response  latency:  0.047
call v: java-spring 10 times

curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 29.968 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 33.625 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 36.291 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 39.107 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.01s system 0% cpu 39.581 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 48.265 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 51.039 total
request time latency is : 51.655211
Cold start: e2e latency:  51.65039291 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.244024
kube-controller pod  latency:  49.582096
call v: java-spring 11 times

curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.202 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 16.099 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 16.598 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 17.102 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 18.376 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 18.890 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 22.967 total
request time latency is : 21.45466
Cold start: e2e latency:  22.956665 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.284294
kube-controller pod  latency:  19.424279
kubelet sandbox  latency:  2.395502
user-container jvm  latency:  6.95
user-container app  latency:  14.71
user-container response  latency:  0.034
call v: java-spring 12 times

curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 13.738 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.087 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 14.861 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 15.500 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 17.935 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 20.377 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.01s system 0% cpu 21.164 total
request time latency is : 19.85843
Cold start: e2e latency:  21.15233489 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.214309
kube-controller pod  latency:  18.410844
kubelet sandbox  latency:  3.438388
call v: java-spring 13 times

curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 12.575 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 13.935 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 14.477 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.525 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 15.186 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 15.647 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 16.399 total
request time latency is : 15.854006
Cold start: e2e latency:  16.38680554 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.263311
kube-controller pod  latency:  14.44758
kubelet sandbox  latency:  2.609652
user-container jvm  latency:  2.37
user-container app  latency:  7.37
user-container response  latency:  0.014
call v: java-spring 14 times

curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.980 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 12.586 total
curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 12.840 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 13.429 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 14.391 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.01s user 0.00s system 0% cpu 15.585 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 15.715 total
request time latency is : 14.713846
Cold start: e2e latency:  15.70209103 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.264191
kube-controller pod  latency:  12.803294
user-container jvm  latency:  1.18
user-container app  latency:  6.91
user-container response  latency:  0.014
call v: java-spring 15 times

curl -s -H "Host: helloworld-php.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 9.955 total
curl -s -H "Host: helloworld-ruby.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 10.699 total
curl -s -H "Host: helloworld-nodejs.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 16.135 total
curl -s -H "Host: helloworld-same-go.default.example.com"  > /dev/null  0.00s user 0.00s system 0% cpu 17.913 total
curl -s -H "Host: helloworld-java-spark.default.example.com"  > /dev/null  0.00s user 0.01s system 0% cpu 18.642 total
curl -s -H "Host: helloworld-python.default.example.com"  > /dev/null  0.01s user 0.00s system 0% cpu 18.741 total
curl -s -H "Host: helloworld-same-java-spring.default.example.com"  >   0.00s user 0.00s system 0% cpu 20.317 total
request time latency is : 19.12399
Cold start: e2e latency:  20.306356 scheduled on ubuntu-node4
kube-scheduler pod  latency:  0.072894
kube-controller pod  latency:  17.174495
kubelet sandbox  latency:  2.358791
